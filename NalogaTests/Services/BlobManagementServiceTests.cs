﻿using Naloga.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Microsoft.WindowsAzure.Storage.Blob;

namespace Naloga.Services.Tests
{
    [TestClass()]
    public class BlobManagementServiceTests
    {
        [TestMethod()]
        public void GetContainersTest()
        {
            var service = new BlobManagementService();
            var result = new List<CloudBlobContainer>(service.GetContainers());

            Assert.IsNotNull(result);
            Assert.AreEqual(result.Count, 3);
        }

        [TestMethod()]
        public void GetBlobsForContainerTest1()
        {
            var service = new BlobManagementService();
            var result = service.GetBlobsForContainer("abc");
            
            Assert.IsNotNull(result);
        }

        [TestMethod()]
        [ExpectedException(typeof(Microsoft.WindowsAzure.Storage.StorageException))]
        public void GetBlobsForContainerTest2()
        {
            var service = new BlobManagementService();
            var result = service.GetBlobsForContainer("tamara");
        }

        [TestMethod()]
        public void GetBlobsForContainerTest3()
        {
            var service = new BlobManagementService();
            var result = new List<CloudBlockBlob>(service.GetBlobsForContainer("bcd"));

            Assert.IsNotNull(result);
            Assert.AreEqual(result.Count, 1);
        }
    }
}