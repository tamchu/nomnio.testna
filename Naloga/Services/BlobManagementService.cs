﻿using Microsoft.Azure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Naloga.Services
{
    public class BlobManagementService : IBlobManagementService
    {
        static CloudStorageAccount storageAccount = CloudStorageAccount.Parse(
            CloudConfigurationManager.GetSetting("StorageConnectionString"));

        CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

        public Task<IEnumerable<CloudBlockBlob>> GetBlobsForContainerAsync(string containerName)
        {
            var task = new Task<IEnumerable<CloudBlockBlob>>(() => GetBlobsForContainer(containerName));
            task.Start();

            return task;
        }

        public Task<IEnumerable<CloudBlobContainer>> GetContainersAsync()
        {
            Task<IEnumerable<CloudBlobContainer>> task = new Task<IEnumerable<CloudBlobContainer>>(() => GetContainers());
            task.Start();

            return task;
        }

        public IEnumerable<CloudBlockBlob> GetBlobsForContainer(string containerName)
        {
            CloudBlobContainer container = blobClient.GetContainerReference(containerName);

            var blobList = new List<CloudBlockBlob>();

            foreach (var blob in container.ListBlobs(null, true))
            {
                if (blob.GetType() == typeof(CloudBlockBlob))
                {
                    var item = (CloudBlockBlob)blob;
                    blobList.Add(item);
                }
            }

            return blobList;
        }

        public IEnumerable<CloudBlobContainer> GetContainers()
        {
            return blobClient.ListContainers();
        }

        public string GetConnectionString()
        {
            return storageAccount.BlobStorageUri.PrimaryUri.ToString();
        }

        public BlobManagementService()
        {

        }
    }
}
