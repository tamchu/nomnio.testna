﻿using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Naloga.Services
{
    public interface IBlobManagementService
    {
        Task<IEnumerable<CloudBlobContainer>> GetContainersAsync();
        Task<IEnumerable<CloudBlockBlob>> GetBlobsForContainerAsync(string containerName);
        string GetConnectionString();
    }
}