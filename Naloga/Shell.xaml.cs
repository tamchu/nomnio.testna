﻿using Naloga.Views;
using Prism.Regions;
using System.Windows;

namespace Naloga
{
    public partial class Shell : Window
    {
        public Shell(IRegionManager regionManager)
        {
            InitializeComponent();
            
            regionManager.RegisterViewWithRegion("ContentRegion", typeof(Containers));
        }

        public Shell()
        {

        }
    }
}
