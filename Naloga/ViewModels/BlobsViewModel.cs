﻿using Naloga.Services;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Regions;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace Naloga.ViewModels
{
    public class BlobsViewModel : BindableBase, INavigationAware
    {
        public ObservableCollection<BlobItem> Blobs
        {
            get { return _blobs; }
            set { SetProperty(ref _blobs, value); }
        }
        
        public string ContainerName
        {
            get { return _containerName; }
            set { SetProperty(ref _containerName, value); }
        }
        
        public string ConnectionString
        {
            get { return _connectionString; }
            set { SetProperty(ref _connectionString, value); }
        }
        
        public DelegateCommand GoBackCommand
        {
            get {

                if (_goBackCommand == null)
                {
                    _goBackCommand = new DelegateCommand(() => {
                        _regionManager.RequestNavigate("ContentRegion", "Containers");
                    });
                }

                return _goBackCommand; }
        }
        
        public BlobsViewModel(IBlobManagementService blobManagement, IRegionManager regionManager)
        {
            _blobManager = blobManagement;
            _regionManager = regionManager;
        }

        public bool IsNavigationTarget(NavigationContext navigationContext)
        {
            return true;
        }

        public void OnNavigatedFrom(NavigationContext navigationContext)
        {
            
        }

        public async void OnNavigatedTo(NavigationContext navigationContext)
        {
            ContainerName = navigationContext.Parameters["containerName"] as string;
            var blobs = await _blobManager.GetBlobsForContainerAsync(ContainerName);

            Blobs = new ObservableCollection<BlobItem>();

            foreach (var i in blobs)
            {
                Blobs.Add(new BlobItem(i.Name, i.StreamWriteSizeInBytes));
            }
            ConnectionString = _blobManager.GetConnectionString();
        }

        #region private fields
        private ObservableCollection<BlobItem> _blobs;
        private IBlobManagementService _blobManager;
        private IRegionManager _regionManager;
        private string _containerName;
        private string _connectionString;
        private DelegateCommand _goBackCommand;
        #endregion

    }

    public class BlobItem
    {
        public string Name { get; set; }
        public double BlobSize { get; set; }

        public BlobItem()
        {

        }

        public BlobItem(string name, long blobSize)
        {
            Name = name;
            BlobSize = ConvertToKb(blobSize);
        }

        private double ConvertToKb(long blobSize)
        {
            return blobSize * 0.008;
        }
    }
}
