﻿using Microsoft.WindowsAzure.Storage.Blob;
using Naloga.Services;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Regions;
using System.Collections.Generic;

namespace Naloga.ViewModels
{
    public class ContainersViewModel : BindableBase, INavigationAware
    {
        public IEnumerable<CloudBlobContainer> Containers
        {
            get { return _containers; }
            set { SetProperty(ref _containers, value); }
        }

        public ContainersViewModel(IBlobManagementService blobManagement, IRegionManager regionManagemer)
        {
            _regionManager = regionManagemer;
            _blobManager = blobManagement;
        }
        
        public DelegateCommand ViewLoadedCommand
        {
            get {

                if (_viewLoadedCommand == null)
                {
                    _viewLoadedCommand = new DelegateCommand(async () => Containers = await _blobManager.GetContainersAsync());
                }
                return _viewLoadedCommand;
            }
        }

        public DelegateCommand ContainerSelectedCommand
        {
            get {
                if (_containerSelectedCommand == null)
                {
                    _containerSelectedCommand = new DelegateCommand(() =>
                    {
                        if (SelectedContainer == null) return;

                        var parameters = new NavigationParameters();
                        parameters.Add("containerName", SelectedContainer.Name);

                        _regionManager.RequestNavigate("ContentRegion", "Blobs", parameters);
                    });
                }

                return _containerSelectedCommand;
            }
        }

        public CloudBlobContainer SelectedContainer {
            get
            {
                return _selectedContainer;
            }

            set
            {
                SetProperty(ref _selectedContainer, value);
            }
        }

        public bool IsNavigationTarget(NavigationContext navigationContext)
        {
            return true;
        }

        public void OnNavigatedFrom(NavigationContext navigationContext)
        {
            
        }

        public async void OnNavigatedTo(NavigationContext navigationContext)
        {
            Containers = await _blobManager.GetContainersAsync();
        }

        #region private fields
        private IEnumerable<CloudBlobContainer> _containers;
        private IRegionManager _regionManager;
        private IBlobManagementService _blobManager;
        private DelegateCommand _viewLoadedCommand;
        private DelegateCommand _containerSelectedCommand;
        private CloudBlobContainer _selectedContainer;
        #endregion
    }
}
