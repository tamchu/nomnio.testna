﻿using Microsoft.Practices.Unity;
using Naloga.Services;
using Naloga.Views;
using Prism.Unity;
using System.Windows;

namespace Naloga
{
    class Bootstrapper : UnityBootstrapper
    {
        protected override DependencyObject CreateShell()
        {
            return Container.TryResolve<Shell>();
        }

        protected override void InitializeShell()
        {   
            Application.Current.MainWindow.Show();
        }

        protected override void ConfigureContainer()
        {
            Container.RegisterTypeForNavigation<Containers>();
            Container.RegisterTypeForNavigation<Blobs>();

            Container.RegisterType<IBlobManagementService, BlobManagementService>();

            base.ConfigureContainer();
        }
    }
}
